import React, { Component } from 'react'

export default class TaskItem extends Component {
  _onUpdateStatus = () => {
    this.props._onUpdateStatus(this.props.task.id);

  }

  _onDelete = () => {
    this.props._onDelete(this.props.task.id);

  }

  _onEdit = () => {
    this.props._onEdit(this.props.task.id);

  }

  render() {
    var { task, index } = this.props;
    return (
      <tr>
        <td>{index + 1}</td>
        <td>{task.name}</td>
        <td className="text-center">
          <span className={task.status === true ? 'btn btn-success' : 'btn btn-danger'}
            onClick={this._onUpdateStatus}>
            {task.status === true ? 'Kích hoạt' : 'Ẩn'}

          </span>
        </td>
        <td className="text-center">
          <button type="button" className="btn btn-warning " onClick={this._onEdit}>
            <span className="" />Sửa
              </button>
          &nbsp;
              <button type="button" className="btn btn-danger" onClick={this._onDelete}>
            <span className="" />Xóa</button>
        </td>
      </tr>

    )
  }
}
