import React, { Component } from 'react'
import Search from './Search'
import Soft from './Soft'

export default class Control extends Component {
    render() {
        return (
            <div className="row mt-15">
                {/* Search */}
                <Search _onSearch={this.props._onSearch} />
                {/* Soft */}
                <Soft _onSoft={this.props._onSoft}
                    _softBy={this.props.softBy}
                    _softValue={this.props.softValue}
                />

            </div>

        )
    }
}
