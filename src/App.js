import React, { Component } from 'react'
import './App.css';
import TaskForm from './Components/TaskForm';
import Control from './Components/Control';
import TaskList from './Components/TaskList';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      isDisplayForm: false,
      taskEdit: null,
      filter: {
        name: '',
        status: -1
      },
      search: '',
      softBy: 'name',
      softValue: 1
    }
  }
  componentWillMount() {
    if (localStorage && localStorage.getItem('tasks')) {
      var tasks = JSON.parse(localStorage.getItem('tasks'));
      this.setState({
        tasks: tasks
      });
    }
  }
  _onDate = () => {
    var tasks = [
      {
        id: this._ramdomId(),
        name: 'Nguyễn Văn Phi',
        status: true
      },
      {
        id: this._ramdomId(),
        name: 'Lê Tùng Khánh',
        status: false
      },
      {
        id: this._ramdomId(),
        name: 'Phan Văn Nhân  ',
        status: true
      },
      {
        id: this._ramdomId(),
        name: 'Đinh Thị Ánh Tuyết  ',
        status: false
      }
    ];

    this.setState({
      tasks: tasks
    });
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  _id() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  _ramdomId() {
    return this._id() + this._id() + '-' + this._id() + '-' + this._id() + '-' + this._id() + '-' + this._id() + '-' + this._id() + '-'
      + this._id() + '-' + this._id();
  }

  _onAddForm = () => {
    if (this.state.isDisplayForm && this.state.taskEdit !== null) {
      this.setState({
        isDisplayForm: true,
        taskEdit: null
      })
    } else
      this.setState({
        isDisplayForm: !this.state.elmTaskForm,
        taskEdit: null
      })
  }

  _onCloseForm = () => {
    this.setState({
      isDisplayForm: false
    })
  }

  _onEditForm = () => {
    this.setState({
      isDisplayForm: true
    })
  }

  _onSubmit = (data) => {
    var { tasks } = this.state;
    if (data.id === '') {
      data.id = this._ramdomId();
      tasks.push(data);
    } else
      var index = this._findIndex(data.id);
    tasks[index] = data;
    this.setState({
      tasks: tasks,
      taskEdit: null
    });
    this._onCloseForm();
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  _onUpdateStatus = (id) => {
    var { tasks } = this.state;
    var index = this._findIndex(id);
    if (index !== -1) {
      tasks[index].status = !tasks[index].status;
      this.setState({
        tasks: tasks
      })
    }
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  _onDelete = (id) => {
    var { tasks } = this.state;
    var index = this._findIndex(id);
    if (index !== -1) {
      tasks.splice(index, 1);
      this.setState({
        tasks: tasks
      })
    }
    localStorage.setItem('tasks', JSON.stringify(tasks));
    this._onCloseForm();
  }

  _onEdit = (id) => {
    var { tasks } = this.state;
    var index = this._findIndex(id);
    var taskEdit = tasks[index]
    this.setState({
      taskEdit: taskEdit
    })
    this._onEditForm();
  }

  _findIndex = (id) => {
    var { tasks } = this.state;
    var result = -1;
    tasks.forEach((task, index) => {
      if (task.id === id) {
        result = index;
      }
    })
    return result;
  }

  _onFilter = (filterName, filterStatus) => {
    console.log(filterName, '-', filterStatus);
    filterStatus = parseInt(filterStatus, 10);
    this.setState({
      filter: {
        name: filterName.toLowerCase(),
        status: filterStatus
      }
    })
  }

  _onSearch = (search) => {
    this.setState({
      search: search.toLowerCase()
    })
  }

  _onSoft = (softBy, softValue) => {
    this.setState({
      softBy: softBy,
      softValue: softValue
    })
    console.log(this.state);
  }

  render() {
    var { tasks, isDisplayForm, taskEdit, filter, search, softBy, softValue } = this.state
    if (filter) {
      if (filter.name) {
        tasks = tasks.filter((task) => {
          return task.name.toLowerCase().indexOf(filter.name) !== -1;
        })
      }
      tasks = tasks.filter((task) => {
        if (filter.status === -1) {
          return task;
        } else {
          return task.status === (filter.status === 1 ? true : false);
        }
      })
    }

    if (search) {
      tasks = tasks.filter((task) => {
        return task.name.toLowerCase().indexOf(search) !== -1;
      })

    }

    if (softBy === 'name') {
      tasks.sort((a, b) => {
        if (a.name > b.name) return softValue;
        else if (a.name < b.name) return -softValue;
        else return 0;
      })
    } else {
      tasks.sort((a, b) => {
        if (a.status > b.status) return -softValue;
        else if (a.status < b.status) return softValue;
        else return 0;
      })
    }

    var elmTaskForm = isDisplayForm ?
      <TaskForm
        onSubmit={this._onSubmit}
        _onCloseForm={this._onCloseForm}
        task={taskEdit}
      /> : '';
    return (
      <div className="container">
        <div className="text-center">
          <h1>Quản Lý Công Việc</h1>
          <hr />
        </div>
        <div className="row">
          <div className={elmTaskForm ? 'col-xs-4 col-sm-4 col-md-4 col-lg-4' : ''}>
            {/* Form*/}
            {elmTaskForm}
          </div>
          <div className={elmTaskForm ? 'col-xs-8 col-sm-8 col-md-8 col-lg-8' : 'col-xs-12 col-sm-12 col-md-12 col-lg-12'}>
            <button type="button" className="btn btn-primary add" onClick={this._onAddForm}>
              <span className="" />Thêm Công Việc
             </button>
            {/* <button type="button" className="btn btn-danger add" onClick={this._onDate}>
              <span className="" />Thêm Công Việc Data
             </button> */}
            {/* Control */}
            <Control
              _onSearch={this._onSearch}
              _onSoft={this._onSoft}
              softBy={softBy}
              softValue={softValue}
            />

            <div className="row mt-15">
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                {/* TaskList */}
                <TaskList tasks={tasks}
                  _onUpdateStatus={this._onUpdateStatus}
                  _onDelete={this._onDelete}
                  _onEdit={this._onEdit}
                  _onFilter={this._onFilter}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
