import React, { Component } from 'react'
import TaskItem from './TaskItem'

export default class TaskList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterName: '',
      filterStatus: -1
    }
  }

  _onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.props._onFilter(
      name === 'filterName' ? value : this.state.filterName,
      name === 'filterStatus' ? value : this.state.filterStatus,
    )
    this.setState({
      [name]: value
    })

  }
  render() {
    var { tasks } = this.props;
    var { filterName, filterStatus } = this.state;
    var elmTasks = tasks.map((task, index) => {
      return <TaskItem key={task.id} index={index} task={task}
        _onUpdateStatus={this.props._onUpdateStatus}
        _onDelete={this.props._onDelete}
        _onEdit={this.props._onEdit}
      />
    })
    return (
      <table className="table table-bordered table-hover mt-15">
        <thead>
          <tr>
            <th className="text-center">STT</th>
            <th className="text-center">Tên</th>
            <th className="text-center">Trạng Thái</th>
            <th className="text-center">Hành Động</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td />
            <td>
              <input type="text"
                className="form-control"
                name="filterName"
                value={filterName}
                onChange={this._onChange}
              />
            </td>
            <td>
              <select className="form-control"
                name="filterStatus"
                value={filterStatus}
                onChange={this._onChange}
              >
                <option value={-1}>Tất Cả</option>
                <option value={0}>Ẩn</option>
                <option value={1}>Kích Hoạt</option>
              </select>
            </td>
            <td />
          </tr>
          {/* TaskItem */}
          {elmTasks}
        </tbody>
      </table>

    )
  }
}
